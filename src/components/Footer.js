import { Row, Col, Container } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Footer() {
  return (
    <div className="font-small mt-2">
      <Container
        fluid
        className="text-center text-md-left border-top border-dark"
      >
        <Row className="mt-3">
          <Col md="6">
            <p>
              &copy; {new Date().getFullYear()} Copyright: Veronica V. Valdez
            </p>
          </Col>
          <Col md="6">
            <p>
              DISCLAIMER: Demo website; all claims and products are fictional
            </p>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
