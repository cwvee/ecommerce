import { useState, useEffect } from "react";
import { Container, Form, Button } from "react-bootstrap";
import { useParams, useHistory } from "react-router-dom";

import Swal from "sweetalert2";

export default function EditProduct() {
  const history = useHistory();
  const { productId } = useParams();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");

  const token = localStorage.getItem("token");
  console.log(token);
  useEffect(() => {
    console.log(productId);
    fetch(`https://boiling-earth-61174.herokuapp.com/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        localStorage.setItem("productId", data._id);
      });
  }, [productId]);

  function editProduct(e) {
    e.preventDefault();
    console.log(productId);

    fetch(
      `https://boiling-earth-61174.herokuapp.com/products/${localStorage.productId}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          productId: productId,
          name: name,
          description: description,
          price: price,
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          setName("");
          setDescription("");
          setPrice("");

          Swal.fire({
            title: "Successfully edited product",
            icon: "success",
            text: "You have successfully edited item info",
          });
          history.push("/admin");
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again",
          });
        }
      });
  }

  const deactivate = (productId) => {
    fetch(
      `https://boiling-earth-61174.herokuapp.com/products/${productId}/archive`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      }
    )
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
      });
    history.push("/products");
  };

  const activate = (productId) => {
    fetch(`https://boiling-earth-61174.herokuapp.com/products/${productId}/unarchive`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
      });
    history.push("/products");
  };

  return (
    <Container fluid className="full-vh">
      <h1 className="text-center py-5">Edit Product</h1>
      <Button variant="primary" onClick={() => deactivate(productId)}>
        Archive item
      </Button>
      <Button
        className="m-4"
        variant="primary"
        onClick={() => activate(productId)}
      >
        Unarchive item
      </Button>
      <Form onSubmit={(e) => editProduct(e)}>
        <Form.Group className="mb-3" controlId="name">
          <Form.Label>Product Name</Form.Label>
          <Form.Control
            type="string"
            placeholder="Enter product name"
            value={name}
            onChange={(e) => setName(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="description">
          <Form.Label>Product Description</Form.Label>
          <Form.Control
            type="string"
            placeholder="Enter product description"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            required
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="price">
          <Form.Label>Price</Form.Label>
          <Form.Control
            type="number"
            placeholder="Enter price"
            value={price}
            onChange={(e) => setPrice(e.target.value)}
            required
          />
        </Form.Group>

        <Button type="submit" id="submitBtn">
          Submit
        </Button>
      </Form>
    </Container>
  );
}
