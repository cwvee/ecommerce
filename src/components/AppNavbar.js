import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";
import Nav from "react-bootstrap/Nav";
import { Link, NavLink } from "react-router-dom";
import { Fragment, useContext } from "react";
import Container from "react-bootstrap/Container";
import UserContext from "../UserContext";

export default function AppNavbar() {
  const { user } = useContext(UserContext);
  return (
    <Navbar
      className="border-top border-bottom border-dark text-uppercase"
      bg="white"
      expand="lg"
    >
      <Container>
        {/* <Navbar.Brand className="logo" as={Link} to="/">
          True Naturals
        </Navbar.Brand> */}
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mx-auto">
            <Nav.Link className="mx-5" as={NavLink} to="/" exact>
              Home
            </Nav.Link>
            <Nav.Link className="mx-5" as={NavLink} to="/products" exact>
              Products
            </Nav.Link>
          </Nav>
          <Nav>
            {user.id !== null ? (
              <NavDropdown
                align="end"
                title={user.firstName}
                id="navbarScrollingDropdown"
              >
                {user.isAdmin === true ? (
                  <NavDropdown.Item as={NavLink} to="/admin" exact>
                    Admin Dashboard
                  </NavDropdown.Item>
                ) : (
                  <Fragment>
                    <NavDropdown.Item as={NavLink} to="/cart" exact>
                      Cart
                    </NavDropdown.Item>
                    <NavDropdown.Item as={NavLink} to="/order-history" exact>
                      Order History
                    </NavDropdown.Item>
                  </Fragment>
                )}

                <NavDropdown.Item as={NavLink} to="/logout" exact>
                  Logout
                </NavDropdown.Item>
              </NavDropdown>
            ) : (
              <Nav.Link as={NavLink} to="/login" exact>
                Sign in
              </Nav.Link>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
