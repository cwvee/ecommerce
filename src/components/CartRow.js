import { Form, Button } from "react-bootstrap";
import { useContext } from "react";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function CartRow({ cartProp, idx }) {
  const { updateCart } = useContext(UserContext);
  const { productName, numberOfItems, price } = cartProp;

  //change quantity of items
  const changeQuantity = (newQuantity) => {
    if (newQuantity !== 0) {
      fetch(`https://boiling-earth-61174.herokuapp.com/users/changeQuantity`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          index: idx,
          numberOfItems: newQuantity,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          if (data === false) {
            Swal.fire({
              title: "Error",
              icon: "error",
              text: "An error occurred. Please try again.",
            });
          } else {
            updateCart(true);
          }
        });
    } else {
      removeProduct(idx);
    }
  };

  const removeProduct = () => {
    fetch(`https://boiling-earth-61174.herokuapp.com/users/removeProduct`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        index: idx,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === false) {
          Swal.fire({
            title: "Error",
            icon: "error",
            text: "Something went wrong",
          });
        } else {
          Swal.fire({
            title: "Success",
            icon: "success",
            text: "Item removed from cart",
          });
          updateCart(true);
        }
      });
  };

  return (
    <tr className="p-4">
      <td>{productName}</td>
      <td>Php {price}</td>
      <td className="text-center">
        <Button
          variant="secondary"
          type="submit"
          onClick={() => changeQuantity(numberOfItems - 1)}
        >
          -
        </Button>
        <Form.Label>{numberOfItems}</Form.Label>
        <Button
          variant="secondary"
          type="submit"
          onClick={() => changeQuantity(numberOfItems + 1)}
        >
          +
        </Button>
      </td>
      <td>Php {price * numberOfItems}</td>
      <td>
        <Button
          type="submit"
          variant="danger"
          className="btn btn-link text-white"
          onClick={() => removeProduct()}
        >
          Remove
        </Button>{" "}
      </td>
    </tr>
  );
}
