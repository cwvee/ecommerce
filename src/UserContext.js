import React from "react";

//create a Context object
const UserContext = React.createContext();

//the "provider" component allows other components to consume/use the context object and supply the necessary information needed to the context object

export const UserProvider = UserContext.Provider;

export default UserContext;
