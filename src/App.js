import { useState, useEffect } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { Route, Switch } from "react-router-dom";
import "./App.css";
import { UserProvider } from "./UserContext";

//components
import AppNavbar from "./components/AppNavbar";
import Footer from "./components/Footer";

//pages
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Landing from "./pages/Landing";
import Register from "./pages/Register";
import Products from "./pages/Products";
import ProductView from "./pages/ProductView";
import OrderHistory from "./pages/OrderHistory";
import Cart from "./pages/Cart";
import Admin from "./pages/Admin";
import EditProduct from "./components/EditProduct";

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  //cart array
  const [cart, setCart] = useState([]);
  const [refreshCart, setRefreshCart] = useState(true);
  const updateCart = (value) => {
    setRefreshCart(value);
  };

  //product Info
  const [productInfo, setProductInfo] = useState({
    productId: "",
    productName: "",
    productDescription: "",
    productPrice: "",
  });

  const retrieveProductDetails = (productId) => {
    fetch(`https://boiling-earth-61174.herokuapp.com/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setProductInfo({
          productId: data._id,
          productName: data.name,
          productDescription: data.description,
          productPrice: data.price,
        });
      });
  };
  const unsetUser = () => {
    localStorage.clear();
  };

  //when page reloads, will fetch the user details by using the token to obtain the data and reset the user state values back to the details
  useEffect(() => {
    fetch(`https://boiling-earth-61174.herokuapp.com/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin,
            firstName: data.firstName,
          });
        } else {
          setUser({
            id: null,
            isAdmin: null,
            firstName: null,
          });
        }
      });
  }, []);

  return (
    <UserProvider
      value={{
        user,
        setUser,
        unsetUser,
        retrieveProductDetails,
        productInfo,
        setProductInfo,
        cart,
        setCart,
        updateCart,
        refreshCart,
      }}
    >
      <Router>
        <h1 className="logo font-Floreal m-0">TRUE NATURALS</h1>
        <AppNavbar />

        <Switch>
          <Route exact path="/">
            <Landing />
          </Route>
          <Route exact path="/login">
            <Login />
          </Route>
          <Route exact path="/logout">
            <Logout />
          </Route>
          <Route exact path="/register">
            <Register />
          </Route>
          <Route exact path="/products">
            <Products />
          </Route>
          <Route exact path="/products/:productId">
            <ProductView />
          </Route>
          <Route exact path="/order-history">
            <OrderHistory />
          </Route>
          <Route exact path="/cart">
            <Cart />
          </Route>
          <Route exact path="/admin">
            <Admin />
          </Route>
          {/* <Route exact path="/addproduct">
            <AddProduct />
          </Route> */}
          <Route exact path="/editproduct/:productId">
            <EditProduct />
          </Route>
        </Switch>

        <Footer />
      </Router>
    </UserProvider>
  );
}

export default App;
