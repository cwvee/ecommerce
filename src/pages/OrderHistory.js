import { Container } from "react-bootstrap";
import Table from "react-bootstrap/Table";
import { Fragment, useState, useEffect } from "react";
import OrderRow from "../components/OrderRow";

export default function OrderHistory() {
  const [orderRow, setOrderRow] = useState([]);

  useEffect(() => {
    fetch(`https://boiling-earth-61174.herokuapp.com/users/myOrders`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setOrderRow(
          data.map((row) => {
            return <OrderRow key={row._id} orderRowProp={row} />;
          })
        );
      });
  }, []);

  return (
    <Container fluid className="full-vh bg-main">
      <h1 className="text-center py-5">Order History</h1>
      <Table hover bordered className="bg-white-transparent">
        <thead>
          <tr>
            <th>ID</th>
            <th>Date</th>
            {/* <th>Items</th>
            <th>Amount</th> */}
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
          <Fragment>{orderRow}</Fragment>
        </tbody>
      </Table>
    </Container>
  );
}
