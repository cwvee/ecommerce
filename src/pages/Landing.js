import { Container, Row, Col } from "react-bootstrap";

import Image from "react-bootstrap/Image";
import {
  HeartIcon,
  BadgeCheckIcon,
  ThumbUpIcon,
  EmojiHappyIcon,
} from "@heroicons/react/outline";
import { Link } from "react-router-dom";

export default function Landing() {
  return (
    <Container fluid className="pb-5 ">
      <Row className="align-items-center vh-90 bg-landing">
        <Col md="6" className="position-relative">
          <div className="mx-5 text-white">
            <h2 className="font-FlorealItalic font-lg">
              Get that glow with True Naturals
            </h2>
            <p>
              Let your skin shine with our all natural and organic skincare
              products
            </p>
            <Link
              className="btn-style py-2 px-3 pe-auto d-inline-block text-dark bg-white"
              to="/products"
            >
              Buy now
            </Link>
          </div>
        </Col>
        <Col md="6"></Col>
      </Row>
      <Row className="my-5 vh-20">
        <Col md="3" className="text-center">
          <HeartIcon className="feature-icon mb-4" />
          <h2 className="feature-text">Cruelty-free</h2>
        </Col>
        <Col md="3" className="text-center">
          <BadgeCheckIcon className="feature-icon mb-4" />
          <h2 className="feature-text">Certified Organic</h2>
        </Col>
        <Col md="3" className="text-center">
          <ThumbUpIcon className="feature-icon mb-4" />
          <h2 className="feature-text">FDA approved</h2>
        </Col>
        <Col md="3" className="text-center">
          <EmojiHappyIcon className="feature-icon mb-4" />
          <h2 className="feature-text">Ethically sourced ingredients</h2>
        </Col>
      </Row>
      <Row className="vh-70 mx-5 g-5">
        <Col md="6" className="img-z img-z1"></Col>
        <Col md="6" className="my-auto px-5">
          <h3 className="font-FlorealItalic text-uppercase font-lg">
            Skincare that works for you
          </h3>
          <p>
            True Naturals prides itself on its ability to create high quality
            and affordable skincare products for every skin concern, so you
            never have to compromise quality with price ever again.
          </p>
        </Col>
      </Row>
      <Row className="vh-70 mt-4 mx-5 g-5">
        <Col md="6" className="my-auto px-5">
          <h3 className="font-FlorealItalic text-uppercase font-lg">
            Ingredients you can trust
          </h3>
          <p>
            Quality skincare starts with quality ingredients, which is why we
            make it our top priority to source sustainable and ethically
            produced ingredients from all over the world.
          </p>
        </Col>
        <Col md="6" className="img-z img-z2"></Col>
      </Row>
    </Container>
  );
}
