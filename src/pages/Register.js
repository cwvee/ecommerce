import { useState, useEffect, useContext } from "react";
import { Form, Button, Container } from "react-bootstrap";
import { Redirect, useHistory } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Register() {
  const { user } = useContext(UserContext);
  const history = useHistory();

  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [isActive, setisActive] = useState(true);

  function registerUser(e) {
    e.preventDefault();

    fetch(`https://boiling-earth-61174.herokuapp.com/users/register`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: password1,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === false) {
          Swal.fire({
            title: "Email already exists",
            icon: "error",
            text: "Please try again.",
          });
        } else {
          // setFirstName("");
          // setLastName("");
          // setEmail("");
          // setPassword1("");
          // setPassword2("");

          Swal.fire({
            title: "Successfully Reistered",
            icon: "success",
            text: "You have succesfully registered",
          });
          history.push("/login");
        }
      });
  }

  useEffect(() => {
    if (
      email !== null &&
      password1 !== null &&
      password2 !== null &&
      password1 === password2 &&
      firstName !== null &&
      lastName !== null
    ) {
      setisActive(true);
    } else {
      setisActive(false);
    }
  }, [email, password1, password2, firstName, lastName]);

  return user.id !== null ? (
    <Redirect to="/products" />
  ) : (
    <Container fluid className="bg-logreg py-5">
      <div className="mx-auto width-50 bg-white-transparent rounded p-5 m-5">
        <Form onSubmit={(e) => registerUser(e)}>
          <h1 className="text-center">Register</h1>
          <Form.Group className="mb-3" controlId="firstName">
            <Form.Label>First Name</Form.Label>
            <Form.Control
              type="string"
              placeholder="Enter first name"
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="lastName">
            <Form.Label>Last Name</Form.Label>
            <Form.Control
              type="string"
              placeholder="Enter last name"
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group className="mb-3" controlId="password1">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={password1}
              onChange={(e) => setPassword1(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="password2">
            <Form.Label>Verify Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Verify Password"
              value={password2}
              onChange={(e) => setPassword2(e.target.value)}
              required
            />
          </Form.Group>
          {isActive ? (
            <Button type="submit" id="submitBtn">
              Submit
            </Button>
          ) : (
            <Button type="submit" id="submitBtn" disabled>
              Submit
            </Button>
          )}
        </Form>
      </div>
    </Container>
  );
}
